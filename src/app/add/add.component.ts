import { Component, OnInit } from '@angular/core';
import {OperationService} from "../operation.service";
import {Operation} from "../entities";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  operation?:Operation;

  constructor(private opService:OperationService) { }

  ngOnInit(): void {
  }

  addOperation() {
    this.opService.add(this.operation!).subscribe();
  }
}
