export class Operation {
  id:number;
  label:string;
  date:Date;
  amount:number;


  constructor(id: number, label: string, date: Date, amount: number) {
    this.id = id;
    this.label = label;
    this.date = date;
    this.amount = amount;
  }
}
