import { Component, OnInit } from '@angular/core';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  list:Operation[] = []
  single?:Operation;
  constructor(private opService:OperationService) { }

  ngOnInit(): void {
    this.opService.getAll().subscribe(data => this.list = data);

  }

  fetchOne() {
    this.opService.getById(2).subscribe(data => this.single = data);
  }

  delete(id:number) {
    this.opService.delete(2).subscribe();
  }
}
