import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {AddComponent} from "./add/add.component";
import {HomeComponent} from "./home/home.component";
import {SingleOperationComponent} from "./single-operation/single-operation.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'operation/:id', component: SingleOperationComponent},
  { path: 'add-operation', component: AddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
