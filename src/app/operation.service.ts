import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Operation } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Operation[]>('http://localhost:8080/api/operations');
  }

  getById(id:number) {
    return this.http.get<Operation>('http://localhost:8080/api/operations/'+id)
  }

  add(operation:Operation) {
    return this.http.post<Operation>('http://localhost:8080/api/operations/', operation)
  }

  delete(id:number) {
    return this.http.delete<Operation>('http://localhost:8080/api/operations/'+id)
  }
}
