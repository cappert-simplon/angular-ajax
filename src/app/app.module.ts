import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import {FormsModule} from "@angular/forms";
import { SearchComponent } from './search/search.component';
import { SingleOperationComponent } from './single-operation/single-operation.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddComponent,
    SearchComponent,
    SingleOperationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
