# Angular-ajax

Créer un projet angular avec ng new que je vais appeler angular-ajax
1. Dans ce projet, créer un fichier src/app/entities.ts avec dedans un type ou une interface pour l'Operation (donc un truc avec un id number, un label string, une date Date et un amount number)
2. Créer un component HomeComponent (avec le ng g c) puis dégager tout ce qu'il y a dans le app.component.html et charger juste son home component à la place
3. Créer un service OperationService (avec le ng g s) et dedans créer une méthode getAll qui va renvoyer un tableau d'opération, pour le moment en dur
4. Injecter le service dans le constructor HomeComponent et s'en servir pour assigner le résultat du getAll à une propriété operations (comme vous avez fait il y a deux semaine à priori)
5. Faire le template du HomeComponent pour afficher les opérations sous forme de liste, ou autre, peu importe
   Jean Demel — Aujourd’hui à 10:28
   Exemple tableau d'opération en dur :
```json   
  [
   {id: 1, label: 'op1', amount: 1, date: new Date()},
   {id: 2, label: 'op2', amount: 2, date: new Date()},
   {id: 3, label: 'op3', amount: 3, date: new Date()},
  ]
```

## Faire un getById depuis angular
1. Dans le service, rajouter une nouvelle méthode getById et dans cette méthode, utiliser le this.http.get pour faire une requête vers le /api/operation/1 et qui va donc renvoyer une Operation plutôt qu'un tableau d'opérations
2. Dans le HomeComponent, créer une propriété single:Operation et faire une méthode fetchOne qui va déclencher le getById du service, subscribe dessus puis assigner le résultat à single
3. Dans le template, rajouter un button qui au click va déclencher le fetchOne, et en dessous de ce bouton, faire en sorte d'afficher les informations de single, si single existe
   annotation pour les en-têtes CORS côté spring, à mettre sur les contrôleurs :
   @CrossOrigin(origins = "http://localhost:4200")

## Les trucs à configurer pour qu'on puisse requêter le serveur depuis angular :
* Angular, rajouter le HttpClientModule dans les imports du AppModule (celui qui vient de @angular/common/http)
* Angular, dans les services qui feront les requêtes, injecter le HttpClient dans le constructor du ou des services en question
* Angular, quand on appelle une méthode d'un service qui fait un appel http, faire un .subscribe() sur cette méthode pour déclencher la requête
* Spring, rajouter les en tête CORS soit avec l'annotation CrossOrigin sur chaque contrôleur, soit avec un fichier de configuration

## Page d'ajout d'opération
1. Créer un nouveau component AddComponent avec le ng g c
2. Dans le AppRoutingModule, rajouter une nouvelle route qui pointera sur ce component (on va dire que la route ça sera 'add-operation')
3. Dans le OperationService, rajouter une méthode add(operation:Operation) qui va effectuer un post vers /api/operation en lui donnant comme deuxième argument l'operation (à part ça, ce sera exactement comme la méthode getAll)
4. Dans le AddComponent, injecter le OperationService (comme on l'a déjà fait dans le HomeComponent) puis créer une méthode addOperation qui va déclencher le add en lui donnant en argument un objet operation en dur
5. Dans le template, rajouter un bouton qui va déclencher la méthode addOperation, et pis tester si ça marche en retournant sur la page d'accueil voir si ça a bien rajouter un truc (modifié)

## Le formulaire
1. Ajouter le FormsModule dans les imports de notre AppModule
2. Dans le AddComponent, rajouter une propriété operation:Operation
3. Dans le template du component, créer un formulaire en assignant les différents input aux propriétés de l'opération via des [(ngModel)] et faire que la méthode addOperation se déclenche au submit du formulaire
4. Modifier le addOperation pour lui faire donner en argument la propriété operation plutôt qu'un objet en dur

## composant de Recherche
1. Créer un nouveau composant SearchComponent
2. dans le template, ajouter un formulaire avec un input texte obligatoire et un bouton de validation
3. dans le component.ts, quand l'utilisateur clique sur le bouton de validation, lancer une requête côté serveur sur le getAll en rajoutant un paramètre "search" dans l'URL. Indice : https://www.tektutorialshub.com/angular/angular-pass-url-parameters-query-strings/
4. Modifier le getAll() dans le OperationController.java (dans le projet SpringBoot), pour prendre en compte un RequestParam optionnel
5. Ajouter une méthode searchByLabel(String filter) dans le OperationRepository.java, pour faire une requête avec un LIKE sur le label
6. Renvoyer les opérations dont le label correspond au filtre donné par l'utilisateur, ou une 404 si aucune ne correspond
